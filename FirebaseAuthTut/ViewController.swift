//
//  ViewController.swift
//  FirebaseAuthTut
//
//  Created by ZEUS on 29/1/20.
//  Copyright © 2020 ZEUS. All rights reserved.
//

import UIKit
import FirebaseAuth

class ViewController: UIViewController {
    let EMAIL = "zahidhasan.9103@gmail.com"
    let PASSWORD = "abcdefghi1234"

    override func viewDidLoad() {
        super.viewDidLoad()
        
         
        
        guard let user = Auth.auth().currentUser else{
            return self.signin(auth: Auth.auth())
        }
        print("Logged in user: \(user.email)")
        
        user.reload { (error) in
                    switch user.isEmailVerified{
            
            case true:
                print("User's email address is verified")
                
            case false:
                user.sendEmailVerification { (error) in
                    guard let error = error else{
                       return print("user email verification is sent")
                    }
                    
                    self.handleError(error: error)
                }
                print("Verify the email address")
            
                
            }
        }
        

        //Delete User
//        user.delete { (error) in
//            guard let error = error else {
//                return print("User was deleted")
//            }
//            self.handleError(error: error)
//        }
    }
    
    func handleError(error: Error){
        //User is not registered
        //User not found
        
        let errorAuthStatus = AuthErrorCode.init(rawValue: error._code)!
        switch errorAuthStatus {
        case .wrongPassword:
            print("wrongPassword")
        case.invalidEmail:
            print("invalidEmail")
        case.operationNotAllowed:
            print("operationNotAllowed")
        case.userDisabled:
            print("userDisabled")
        case.userNotFound:
            print("userNotFound")
            self.register(auth: Auth.auth())
        case.tooManyRequests:
            print("Too many request, oppss")
        default:
            fatalError("error not supported here")
        }
        
    }
    
    func signin(auth: Auth){
        
        auth.signIn(withEmail: EMAIL , password: PASSWORD) { (result, error) in
            
            guard  error == nil else{
                return self.handleError(error: error!)
            }
            
            guard let user = result?.user else{
                fatalError("Not User do not know what went wrong")
            }
            print("Signed in user: \(user.email)")
        }
    }
    
    func register (auth: Auth){
        auth.createUser(withEmail: EMAIL, password: PASSWORD) { (result, error) in
            guard error == nil else {
                return self.handleError(error: error!)
            }
            
            guard let user = result?.user else{
                fatalError("Dont know why this happend")
            }
            
            print("registered user: \(user.email)")
        }
    }



}

